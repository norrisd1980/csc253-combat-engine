﻿namespace CombatEngineV1_Sample
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblOutput = new System.Windows.Forms.Label();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnRunCombat = new System.Windows.Forms.Button();
            this.btnLoadMobs = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblOutput
            // 
            this.lblOutput.AutoSize = true;
            this.lblOutput.Location = new System.Drawing.Point(230, 8);
            this.lblOutput.MinimumSize = new System.Drawing.Size(500, 350);
            this.lblOutput.Name = "lblOutput";
            this.lblOutput.Size = new System.Drawing.Size(500, 350);
            this.lblOutput.TabIndex = 0;
            this.lblOutput.Text = "Combat Engine Output";
            this.lblOutput.Click += new System.EventHandler(this.lblOutput_Click);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(66, 105);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 30);
            this.btnReset.TabIndex = 1;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            // 
            // btnRunCombat
            // 
            this.btnRunCombat.Location = new System.Drawing.Point(66, 41);
            this.btnRunCombat.Name = "btnRunCombat";
            this.btnRunCombat.Size = new System.Drawing.Size(75, 23);
            this.btnRunCombat.TabIndex = 2;
            this.btnRunCombat.Text = "Run Combat";
            this.btnRunCombat.UseVisualStyleBackColor = true;
            // 
            // btnLoadMobs
            // 
            this.btnLoadMobs.Location = new System.Drawing.Point(66, 198);
            this.btnLoadMobs.Name = "btnLoadMobs";
            this.btnLoadMobs.Size = new System.Drawing.Size(75, 23);
            this.btnLoadMobs.TabIndex = 3;
            this.btnLoadMobs.Text = "Load Mobs";
            this.btnLoadMobs.UseVisualStyleBackColor = true;
            this.btnLoadMobs.Click += new System.EventHandler(this.btnLoadMobs_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 367);
            this.Controls.Add(this.btnLoadMobs);
            this.Controls.Add(this.btnRunCombat);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.lblOutput);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblOutput;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnRunCombat;
        private System.Windows.Forms.Button btnLoadMobs;
    }
}

