﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CombatEngineV1_Sample
{
    // first pass at simple combat engine. 
    // essentially stateless -- track changes by observing the Attacker and Defender
    // provides string feedback in text

    class CombatEngine
    {
        // DEBUG FLAG
        const bool IS_DEBUG = true;
        // const bool IF_DEBUG = false;

        private static string NEWLINE = "\n";
        private static int BASE_DEFENSE = 10;
        private Die d6_1, d6_2, d6_3; // 3d6 attack roll

        private string _outputString;
        private string OutputString
        { 
            get
            {
                return _outputString;
            }
        }
        public Mob Attacker { get; set; }
        public Mob Defender { get; set; }

        public CombatEngine()
        {
            _outputString = "Combat Engine Initialized\n";
            d6_1 = new Die(6);
            d6_2 = new Die(6);
            d6_3 = new Die(6);
        }

        public void Init(Mob attacker, Mob defender)
        {
            this.Attacker = attacker;
            this.Defender = defender;
        }

        // DoCombatRound - carries out one full round
        // returns false if round could not be completed, true otherwise
        // check state of Attacker, Defender, OutputString
        public bool DoCombatRound()
        {
            
            if (this.Attacker == null)
            {
                appendMessage("Error: No Attacker property set");
                return false;
            }
            if (this.Defender == null)
            {
                appendMessage("Error: No Defender property set");
                return false;
            }
            // Combat round steps
            // A attacks D
            // if attack bonus + attack roll are higher than defense (base defense + defense bonus), hit
            // (attack roll is 3d6, base defense is 10)
            // if hit, reduce defender's HP
            // in this version, you need to manually swap the attacker and defender for both sides to fight each other.
            int atkRoll, defRoll, atkDmg;

            // attack = 3d6+attack bonus
            atkRoll = Attacker.AttackBonus + roll3d6();
            appendMessage(Attacker.Name + " attacks, rolling " + atkRoll);

            defRoll = BASE_DEFENSE + Defender.DefenseBonus;
            appendMessage("versus " + Defender.Name + "'s defense of " + defRoll);

            if (atkRoll > defRoll)
            {
                Attacker.DamageDie.roll();
                atkDmg = Attacker.DamageDie.Result + Attacker.DamageMod - Defender.ArmorFactor;
                Defender.HitPoints = Defender.HitPoints - atkDmg;
                appendMessage("Hit! " + Defender.Name + "takes " + atkDmg + "damage. Reduced to " + Defender.HitPoints + "HP.");
                if (Defender.HitPoints <= 0) {
                    appendMessage(Defender.Name + " is slain.");
                }
            }
            else
            {
                appendMessage("Miss!");
            }
            return true;
        }

        private int roll3d6()
        {
            d6_1.roll();
            d6_2.roll();
            d6_3.roll();
            return d6_1.Result + d6_2.Result + d6_3.Result;
        }

        private void appendMessage(string newMsg)
        {
            _outputString = _outputString + newMsg + NEWLINE;
            if (IS_DEBUG)
            {
                Console.WriteLine(newMsg + NEWLINE);
            }
        }

    }
}
