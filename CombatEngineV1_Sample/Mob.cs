﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CombatEngineV1_Sample
{
    // MOB stands for "mobile object" (from DikuMUD)
    // in this version all logic is handled at a higher level in CombatEngine
    class Mob
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int HitPoints { get; set; }
        public int DefenseBonus { get; set; }
        public int AttackBonus { get; set; }
        public Die DamageDie { get; set; } 
        public int DamageMod { get; set; }
        public int ArmorFactor { get; set; }

        public Mob()
        {
   
        }
    }
}
