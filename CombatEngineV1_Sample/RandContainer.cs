﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CombatEngineV1_Sample
{
    class RandContainer
    {
        // Singleton used to hold a common Random object instance
        private static RandContainer _instance;
        private Random _rand;

        private RandContainer()
        {
            // a singleton is not accessed by creating a new object
            // hence the private constructor
            // however the constructor is still called
            _rand = new Random();
        }

        public static RandContainer Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new RandContainer();
                }
                return _instance;
            }
        }

        public Random Rand
        {
            get
            {
                return _rand;
            }
        }

        public void setRandomSeed(int seed)
        {
            _rand = new Random(seed);
        }
    }
}
