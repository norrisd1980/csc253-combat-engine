﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CombatEngineV1_Sample
{
    // random die ("dice" is plural)
    // usage: set Size prop, call roll() method, get Result prop
    //
    class Die
    {
        
        private int _dieSize, _dieResult;
        private RandContainer _rc;

        public Die(int dieSize = 6)
        {
            // default die size is d6
            _dieSize = dieSize;

            _rc = RandContainer.Instance; // any seed decisions should be made at a higher level
        }

        public int Size
        {
            set
            {
                _dieSize = value;
            }
            get
            {
                return _dieSize;
            }
        }

        public int Result
        {
            // no setter, die.Result is a read only property
            get
            {
                return _dieResult;
            }
        }

        public void roll()
        {
            _dieResult = _rc.Rand.Next(_dieSize) + 1; // random.Next(n) is 0 to n-1, so add one
        }



    }
}
