﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace CombatEngineV1_Sample
{
    public partial class Form1 : Form
    {
        CombatEngine _engine;
        Mob _enemy;
        Player _pl;

        public Form1()
        {
            InitializeComponent();

            // init engine
            _engine = new CombatEngine();
            _enemy = new Mob();
            _pl = new Player();

            _enemy.HitPoints = 10;
            _pl.HitPoints = 20;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void lblOutput_Click(object sender, EventArgs e)
        {

        }

        private void btnLoadMobs_Click(object sender, EventArgs e)
        {
            // load the enemy mobs file, then select the first one in the list to fight
            // todo: select other mobs

            List<Mob> mobList = new List<Mob>();

            StreamReader stream = new StreamReader("c:\\dev\\mobs.txt");
            

            using (stream)
            {
                int lineNumber = 0;
                string thisLine = "";

                while (thisLine != null)
                {
                    Mob newMob = new CombatEngineV1_Sample.Mob();
                    string[] lineItems;

                    // read name
                    lineNumber++;
                    thisLine = readLine(stream, lineNumber);
                    lineItems = thisLine.Split(':');
                    newMob.Name = lineItems[1];

                    // read description
                    lineNumber++;
                    thisLine = readLine(stream, lineNumber);
                    lineItems = thisLine.Split(':');
                    newMob.Description = lineItems[1];

                    // read HP
                    lineNumber++;
                    thisLine = readLine(stream, lineNumber);
                    lineItems = thisLine.Split(':');
                    newMob.HitPoints = int.Parse (lineItems[1]);

                    // ... 
                    // read and discard the empty line between mobs
                    thisLine = readLine(stream, lineNumber);

                    // finally add the mob to the list
                    mobList.Add(newMob);
                
                }

                _enemy = mobList[0]; // treating the list as an array!

            }

        }
        private string readLine(StreamReader stream, int lineNumber)
        {

            string thisLine = stream.ReadLine();
            // if debug flag goes here
            Console.WriteLine("Line {0}: {1}", lineNumber, thisLine);
            return thisLine;  
        }
    }
}
